<?php

namespace Pukudada\Loyalty\Traits;

use Illuminate\Database\Eloquent\Builder;



trait Multitenantable
{
    public static function bootMultitenantable()
    {
        static::creating(function ($model) {
            if (starts_with(request()->path(), 'api')) {
                $model->restaurant_id = request()->headers->get('restaurant_id');
            } else if (auth()->check()) {
                if (auth()->user()->user_type == 2)
                    $model->restaurant_id = request()->restaurant_id;
                else
                    $model->restaurant_id = auth()->user()->restaurant_id;
            } elseif(isset(request()->restaurant_id))
                $model->restaurant_id = request()->restaurant_id;

        });
        static::addGlobalScope('restaurant_id', function (Builder $builder) {
            if (starts_with(request()->path(), 'api')) {
                $restaurant_id = request()->headers->get('restaurant_id');
                return $builder->where('restaurant_id', $restaurant_id);
            }
            if (auth()->check() && \auth()->user()->user_type != 2)
                return $builder->where('restaurant_id', auth()->user()->restaurant_id);
            if (request()->restaurant_id != null)
                return $builder->where('restaurant_id', request()->restaurant_id);
        });
    }
}

?>
