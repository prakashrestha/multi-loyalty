<?php
return [

    'loyalty_Model' => 'App\CustomerOrderLoyalty',

    'user_Model' => 'App\Restaurant\Models\ro_user',
    'order_Model' => 'App\Restaurant\Models\ro_order',
    'restaurant_Model' => 'App\Restaurant\Models\ro_restaurant',


    'users_table' => 'ro_users',
    'loyalty_table' => 'loyalty',
    'restaurant_table' => 'ro_restaurants',
    'customer_loyalty_table' => 'customer_loyalty',
    'order_table' => 'ro_orders',


    'user_foreign_key' => 'user_id',

    'order_foreign_key' => 'order_id',
];
