<?php
namespace Pukudada\Loyalty\Facades;

use Illuminate\Support\Facades\Facade;

class LoyaltyFacades extends Facade {
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'loyalty';
    }
}
