<?php


namespace Pukudada\Loyalty;


use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class Loyalty
{

    public static function settingLoyalty()
    {
        $loyalty_setting = LoyaltySetting::first();

        return $loyalty_setting;
    }

    public static function settingUpdate($enable, $expense_amt, $loyalty_conversion, $points, $monetary_conversion, $redeem_threshold)
    {
        try {

            $loyalty_setting = LoyaltySetting::first();

            $loyalty_setting->expense_amt = $expense_amt ?? $loyalty_setting->expense_amt;
            $loyalty_setting->loyalty_conversion = $loyalty_conversion ?? $loyalty_setting->loyalty_conversion;
            $loyalty_setting->points = $points ?? $loyalty_setting->points;
            $loyalty_setting->enable = $enable ;
            $loyalty_setting->monetary_conversion = $monetary_conversion ?? $loyalty_setting->monetary_conversion;
            $loyalty_setting->redeem_threshold = $redeem_threshold ?? $loyalty_setting->redeem_threshold;
            $loyalty_setting->save();
            return $loyalty_setting->refresh();
        } catch (\Exception $e) {

        }
    }

    public static function store($user_id, $order_id, $amount)
    {
        try {
            $amount = $amount/100; // convert to dollar from cent
            $loyalty_setting = LoyaltySetting::first();
            $point = round(($amount / $loyalty_setting->expense_amt) * $loyalty_setting->loyalty_conversion);
            $array = [
                'user_id' => $user_id,
                'order_id' => $order_id,
                'loyalty_points' => $point,

            ];
            $ret = CustomerOrderLoyalty::create($array);
            if ($ret) {
                $cus_loyalty = CustomerLoyalty::where('user_id', $user_id)->first();
                if ($cus_loyalty != null) {
                    $cus_loyalty->loyalty_total += $point;
                    $cus_loyalty->loyalty_remain += $point;
                    $cus_loyalty->save();
                } else {
                    $data = [
                        'user_id' => $user_id,
                        'loyalty_total' => $point,
                        'loyalty_remain' => $point,
                    ];
                    $cus_loyalty = CustomerLoyalty::create($data);
                }
            }

            return $ret;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public static function AdminLoyaltyList($request = null, $res_id = null)
    {
        //request parameter must be "filter"
        try {
            if ($request->email || $request->phone) {
                $customer = self::searchCustomerLoyalty($request->email??null, $request->phone??null, $res_id );
                $cus_loyalty = [];
                if($customer['code'] === 200) {
                    $cus_loyalty = CustomerLoyalty::where('user_id', $customer['data']['customer_id'])
                        ->get();
                }
            }else
                $cus_loyalty = CustomerLoyalty::all();
            $array = [];
            foreach ($cus_loyalty as $item) {
                $userModel = config('loyalty_config.user_Model', 'App\Restaurant\Models\ro_user');
                $customer = $userModel::find($item->user_id);
                $array[] = [
                    'loyalty_total' => $item->loyalty_total,
                    'redeem_point' => $item->redeem_point,
                    'redeem_at' => $item->redeem_at,
                    'loyalty_remain' => $item->loyalty_remain,
                    'customer' => $customer,
                ];

            }
            return $array;
        } catch (\Exception $e) {
            Log::error($e);
            return $e->getMessage();
        }

    }

    public static function LoyaltyDetail($id)
    {
        try {
            $cus_order_loyalty = CustomerOrderLoyalty::where(['user_id' => $id])->latest()->get();
            return $cus_order_loyalty;
        } catch (\Exception $e) {
            Log::error($e);
            return $e->getMessage();
        }
    }

    public static function currentLoyalty($customer_id)
    {
        try {
            $array = [
                'current_point' => 0,
                'current_value' => 0,
                'threshold' => 0
            ];
            $loyalty_setting = LoyaltySetting::first();
            $cus_loyalty = CustomerLoyalty::where('user_id', $customer_id)->first();

            if ($cus_loyalty)
                $array = [
                    'current_point' => (float) round($cus_loyalty->loyalty_remain),
                    'current_value' => (float) number_format((($cus_loyalty->loyalty_remain / $loyalty_setting->points) * $loyalty_setting->monetary_conversion),2),

                ];
            $array['threshold'] = (float) $loyalty_setting->redeem_threshold;
            return $array;

        } catch (\Exception $e) {
            Log::error($e);
            return $e->getMessage();
        }


    }

    public static function redeem($customer_id, $order_id, $amount, $loyaltydetail)
    {

        try {
            $ret_array = [];
            $loyalty_setting = LoyaltySetting::first();
            $cus_loyalty = CustomerLoyalty::where('user_id', $customer_id)->first();
            if ($loyalty_setting->redeem_threshold <= $cus_loyalty->loyalty_remain) {
                $redeem = round(($loyaltydetail['current_point'] / $loyalty_setting->points) * $loyalty_setting->monetary_conversion, 2);

                $cumulative_points = round(($amount /  $loyalty_setting->expense_amt) * $loyalty_setting->loyalty_conversion);

                if ($redeem > $amount) {
                    $reducing_points = round(($amount / $loyalty_setting->monetary_conversion) * $loyalty_setting->points);
                    $payable_amt = false;
                    $redeem_value = $amount;

                } elseif ($amount >= $redeem) {
                    $reducing_points = round($loyaltydetail['current_point']);
                    $payable_amt = ($amount - $redeem >= 0) ? $amount - $redeem : false;
                    $redeem_value = $redeem;
                }
                $array = [
                    'user_id' => $customer_id,
                    'order_id' => $order_id,
                    'redeem_points' => $reducing_points,
                    'loyalty_points' => $cumulative_points,
                    'redeem_value' => number_format($redeem_value, 2),   //monetary value

                ];
                $ret = CustomerOrderLoyalty::create($array);
                if ($ret) {
                    $cus_loyalty->redeem_point += $reducing_points;
                    $cus_loyalty->loyalty_total += $cumulative_points;
                    $cus_loyalty->loyalty_remain = round($cus_loyalty->loyalty_remain - $reducing_points + $cumulative_points);
                    $cus_loyalty->save();
                }

                $ret_array =  ['payable_amt' => $payable_amt,
                    'redeem_point' => $reducing_points,
                    'redeem_amt'=> number_format($redeem_value, 2), ];
                return $ret_array;
            } else {
                return "Loyalty couldn't be redeemed as You haven\'t enough points.";
            }

        } catch (\Exception $e) {
            Log::error($e);
            return $e->getMessage();
        }


    }

    public static function check($customer_id, $amount, $loyaltydetail)
    {
        try {
            $loyalty_setting = LoyaltySetting::first();
            $cus_loyalty = CustomerLoyalty::where('user_id', $customer_id)->first();
            if($cus_loyalty) {
                if ($loyalty_setting->redeem_threshold <= $cus_loyalty->loyalty_remain) {
                    $redeem = ($loyaltydetail['current_point'] / $loyalty_setting->points) * $loyalty_setting->monetary_conversion;
                    if ($redeem > $amount) {
                        $reducing_points = round(($amount / $loyalty_setting->monetary_conversion) * $loyalty_setting->points);
                        $payable_amt = false;
                        $response = [
                            'code' => 200,
                            'data' => $payable_amt,
                            'message' => 'Redeem amount is greater than payable amount'];

                    } elseif ($amount >= $redeem) {
                        $reducing_points = round($loyaltydetail['current_point']);
                        $payable_amt = ($amount - $redeem >= 0) ? $amount - $redeem : false;
                        $response = [
                            'code' => 201,
                            'data' => number_format($payable_amt, 2),
                            'message' => 'Redeem amount is less than payable amount'];
                    }
                } else {
                    $response = [
                        'code' => 202,
                        'data' => null,
                        'message' => 'Loyalty couldn\'t be redeemed as you haven\'t enough points .'];

                }
            } else {
                $response = [
                    'code' => 202,
                    'data' => null,
                    'message' => 'No loyalty points found!'];
            }
            return $response;

        } catch (\Exception $e) {
            Log::error($e);
            return $e->getMessage();
        }


    }

    public static function searchCustomerLoyalty($email = null,$phone = null, $res_id)
    {
        try {
            $userModel = config('loyalty_config.user_Model', 'App\Restaurant\Models\ro_user');
            $orderModel = config('loyalty_config.order_Model', 'App\Restaurant\Models\ro_order');
            $email_cust = null;
            if ($email != null) {
                $email_cust = $userModel::where('email', str_replace(' ', '', $email))->where('restaurant_id', $res_id)->first();
            }
            $telephone = null;
            if ($phone != null) {
                $telephone = $orderModel::where('telephone', $phone)->where('restaurant_id', $res_id)->whereNotNull('customer_id')->first();
            }

            $customer = null;
            if ($email_cust)
                $customer = $email_cust->id;
            if ($telephone)
                $customer = $telephone->customer_id;

            $searchBy = $email != null ? 'email' : 'phone';
            if ($customer) {
                $loyalty = self::currentLoyalty($customer);
                $loyalty['customer_id'] = $customer;

                return $response = [
                    'code' => 200,
                    'data' => $loyalty,
                    'message' => 'Customer with provided '.$searchBy.' have "' . $loyalty['current_point'] . '" loyalty points with monetary value of $ '. number_format($loyalty['current_value'],2)];

            } else
                return $response = [
                    'code' => 205,
                    'data' => null,
                    'message' => 'No customer found with this '.$searchBy.'.'];
        }catch (\Exception $e){
            Log::error($e);
            return $e->getMessage();
        }
    }

    public static function adminRedeemLoyalty($customer_id, $amount)
    {
        try {
            $ret_array = ['payable' => 0, 'reduced' => 0];

            $loyalty_setting = LoyaltySetting::first();
            $loyaltydetail = self::currentLoyalty($customer_id);
            $cus_loyalty = CustomerLoyalty::where('user_id', $customer_id)->first();
            $check = self::check($customer_id, $amount, $loyaltydetail);
            $redeem = ($loyaltydetail['current_point'] / $loyalty_setting->points) * $loyalty_setting->monetary_conversion;

            if ($check['code'] === 200) {
                $reducing_points = round(($amount / $loyalty_setting->monetary_conversion) * $loyalty_setting->points);
                $payable_amt = false;
                $redeem_value = $amount;
            } elseif ($check['code'] === 201) {
                $reducing_points = round($loyaltydetail['current_point']);
                $payable_amt = ($amount - $redeem >= 0) ? $amount - $redeem : false;
                $redeem_value = $redeem;
            } else {
                return $response = [
                    'code' => 203,
                    'data' => null,
                    'message' => 'Not enough Points to redeem'];
            }

            $array = [
                'user_id' => $customer_id,
                'order_id' => null,
                'manual_loyalty' => 1,
                'loyalty_points' => 0,
                'redeem_points' => $reducing_points,
                'redeem_value' => number_format($redeem_value, 2),   //monetary value

            ];
            $ret = CustomerOrderLoyalty::create($array);
            if ($ret) {
                $cus_loyalty->redeem_point += $reducing_points;
                $cus_loyalty->loyalty_remain = round($cus_loyalty->loyalty_remain - $reducing_points);
                $cus_loyalty->save();
            }

            $ret_array = ['payable' => $payable_amt, 'reduced' => $reducing_points];
            if ($payable_amt === false) {
                $message = 'Point has been used and deducted';
            } elseif ($payable_amt > 0) {
                $message = 'Point has been reduced and customer should pay additional $ ' . $payable_amt;
            }
            return $response = [
                'code' => 200,
                'data' => $ret_array,
                'message' => $message];
            return $response;

        } catch (\Exception $e) {
            Log::error($e);
            return $e->getMessage();
        }
    }

    public static function removeLoyaltyPoint($order_id)
    {
        $ret = CustomerOrderLoyalty::where(['order_id'=> $order_id])->first();
        $loyalty_customer = CustomerLoyalty::where(['user_id'=>$ret->user_id])->first();
        $loyalty_customer->loyalty_total = $loyalty_customer->loyalty_total - $ret->loyalty_points;
        $loyalty_customer->redeem_point = $loyalty_customer->redeem_point  - $ret->redeem_points;
        $loyalty_customer->loyalty_remain = $loyalty_customer->loyalty_remain  + $ret->redeem_points - $ret->loyalty_points;
        $loyalty_customer->save();
        $ret->delete();
        return true;
    }

    public static function checkIfLoyaltyEnabled($restaurant_id){

        $setting = LoyaltySetting::where('restaurant_id',$restaurant_id)->first();
        if ($setting->enable == 1)
            return true;
        else
            return false;
    }

    public static function getRedeemData($order_id)
    {
        $redeem_data = [];
        $col = CustomerOrderLoyalty::where(['order_id' => $order_id])->first();
        if ($col) {
            $redeem_data = [
                'redeem_point' => $col->redeem_points,
                'redeem_amt' => number_format($col->redeem_value, 2),
            ];
        }
        return $redeem_data;
    }

}
