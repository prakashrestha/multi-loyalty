<?php

namespace Pukudada\Loyalty;

use Illuminate\Support\ServiceProvider;

class LoyaltyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('loyalty', 'Pukudada\Loyalty\Loyalty');

//        $this->app->make('Pukudada\Loyalty\LoyaltyController');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/migration');
//        $this->loadViewsFrom(__DIR__.'/views', 'loyalty');
        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/loyalty'),
        ], 'public');
        $this->publishes([
            __DIR__.'/migration/' => database_path('migrations')
        ], 'migration');
        $this->publishes([
            __DIR__.'/config/loyalty_config.php' => config_path('loyalty_config.php')
        ], 'config');
    }
}
