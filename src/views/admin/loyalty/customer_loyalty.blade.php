@extends('layouts.app')

@section('content')
	<div class="ui main container">
		<div class="heading-section">
			<h1 class="page-heading">Loyalty Detail</h1>
		</div>
		
		<div class="main-container">
			<table class="ui celled striped table item-list-with-image customer-list-table">
				<thead>
				<tr>
					<th>SN</th>
					<th>Order Id</th>
					<th>Order Loyalty points</th>
					<th>Redeem Points</th>
					<th>Redeem value</th>
					<th>Created At</th>
				</tr>
				</thead>
				<tbody>
				@if($loyalty_list->total() != 0)
					@foreach($loyalty_list as $key=>$loyalty)
						<tr>
							<td>{{$key+1}}</td>
							<td>{{$loyalty->order_id}}</td>
							<td>{{$loyalty->loyalty_points}}</td>
							<td>{{$loyalty->redeem_points}}</td>
							<td>{{$loyalty->redeem_value}}</td>
							<td>{{$loyalty->created_at}}</td>
							
						</tr>
					@endforeach
				@else
					<tr><td colspan="5"><p>No Loyalty found.</p></td></tr>
				@endif
				</tbody>
				<tfoot>
				@if($loyalty_list->total() > 0)
					<tr>
						<td colspan="3">
							<p class="pagination-status">Showing {{ $loyalty_list->firstItem() }} - {{ $loyalty_list->lastItem() }} of {{ $loyalty_list->total() }} items.</p>
						</td>
						<td colspan="3">
							<div>
								<div class="ui right floated per-page-section">
									Per Page &nbsp;
									<div class="ui selection dropdown customer-per-page">
										<input type="hidden" name="">
										<i class="dropdown icon"></i>
										<div class="default text">N</div>
										<div class="menu">
											<div class="item" data-value="10">10</div>
											<div class="item" data-value="25">25</div>
											<div class="item" data-value="50">50</div>
											<div class="item" data-value="100">100</div>
										</div>
									</div>
									&nbsp;items
								</div>
							</div>
						</td>
					</tr>
				@endif
				<tr class="list-table-footer-row">
					<td colspan="4">
					
					</td>
					<td colspan="2">
						@if($loyalty_list->lastPage() > 1)
							{{ $loyalty_list->links() }}
						@endif
					</td>
				</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<script>
        var customerPerPage = '<?php echo $filter['perPage']; ?>';
	
	</script>
@endsection
