<?php

namespace Pukudada\Loyalty;

use Illuminate\Database\Eloquent\Model;
use Pukudada\Loyalty\Traits\Multitenantable;

class CustomerOrderLoyalty extends Model
{
    use Multitenantable;
    protected $table = 'customer_order_loyalty';

    protected $guarded = [];
}
