<?php

namespace Pukudada\Loyalty;

use Illuminate\Database\Eloquent\Model;
use Pukudada\Loyalty\Traits\Multitenantable;

class LoyaltySetting extends Model
{
    use Multitenantable;

    protected $table = 'loyalty_setting';

    protected $guarded = [];
}
