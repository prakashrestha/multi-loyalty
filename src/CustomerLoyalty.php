<?php

namespace Pukudada\Loyalty;

use Illuminate\Database\Eloquent\Model;
use Pukudada\Loyalty\Traits\Multitenantable;

class CustomerLoyalty extends Model
{
    use Multitenantable;

    protected $table = 'customer_loyalty';

    protected $guarded = [];
}
