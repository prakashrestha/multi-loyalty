

## About this package

- Loyalty for every expenses the client made.
- redeem on meeting the loyalty threshold.




## Installation

To install the package you can run following in your terminal.

`composer require pukudada/loyalty`

or add following to your composer.json file and run `composer update`.

        "require": {
             ...
             "pukudada/loyalty": "1.5.6",
             ...
        }


This will install loyalty package in your project. Once completed, we need to add this code in 'config/app.php'.


    'aliases' => [
            ...
            'Loyalty' => Pukudada\Loyalty\Facades\LoyaltyFacades::class,
            ...
    ];

After success installation of the package, we need to import the migrations and config file required for the loyalty. for this please runfollowing two command in terminal.

`php artisan vendor:publish --tag=migration`

`php artisan vendor:publish --tag=config`

This will import require migration files to the project. Now final step we are to migrate the imported migration file.

`php artisan migrate`

## License

The is open-sourced package licensed under the [MIT license](https://opensource.org/licenses/MIT).
